import calc from '../src/calc.js';
import { divide } from '../src/calc.js';
import { expect, assert, should } from 'chai';

describe('Unit testing calc.js', () => {
    let myvar = undefined;
    before(() => {
        myvar = 1; // setup before testing
        console.log('Before testing');
    });
    it('Should add two numbers together.', () => {
        expect(calc.add(1, 2)).to.equal(3);
        expect(calc.add(2, 2)).to.equal(4);
    });
    it('Add function should return string when either of the paremeters is not number', () => {
        expect(calc.add(1, 'b')).to.be.a('string')
        expect(calc.add('b', 2)).to.be.a('string')
    });
    it('Should subtract correctly', () => {
        assert((calc.subtract(4, 2) == 2), 'Should equal 4-2 = 2');
        assert((calc.subtract(10, 5) == 5), 'Should equal 10-5 = 5');
    });

    it('Subtraction parameters should be a numbers', () => {
        assert.isNaN(calc.subtract(4, 'b'));
        assert.isNaN(calc.subtract('a', 4));
    });
    // Can't figure out why removing should.exists() test breaks other should tests.
    it('Mupltiply function should exist', () => {
        should().exist(calc.multiply); 
    });
    it('Mupltiply function should multiply two numbers correctly', () => {
        (calc.multiply(5, 3)).should.equal(15); 
        (calc.multiply(4, 7)).should.equal(28); 
    });
    it('Should throw an error when either of the parameters is not a number', () => {

        (() => {
    
          calc.multiply('a', 3);
    
        }).should.throw('Both parameters must be numbers');
    
    
        (() => {
    
            calc.multiply(2, 'g');
    
        }).should.throw('Both parameters must be numbers');
    
    
        (() => {
    
            calc.multiply('a', 'b');
    
        }).should.throw('Both parameters must be numbers');
    
    });
    it('Should throw an error when the divisor is 0', () => {

        expect(() => {
    
            divide(1, 0);
    
        }).to.throw('Division by zero error!');
    
    });
    
    // it('Random', () => expect(Math.random()).to.above(0.5)); // test randomly fails
    it.skip('Random', () => expect(Math.random()).to.above(0.5)); // skip is better than commenting
    after(() => console.log('After calc tests')); // runs after all tests
});
