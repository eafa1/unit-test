// src/calc.js
/**
* Adds two numbers together
* @param {number} a
* @param {number} b
* @returns {number}
*/
const add = (a, b) => a + b;
/**
* Subtracts number from minuend
* @param {number} minuend
* @param {number} subtrahend
* @returns {number} difference
*/
const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
}
/**
* This function calculates product between two numbers.
* @param {number} multiplier number used for multiplication (factor)
* @param {number} multiplicant number being multiplied
* @returns {number} product
*/
const multiply = (multiplier, multiplicant) => {
    if(isNaN(multiplier) || isNaN(multiplicant)){
        throw new Error('Both parameters must be numbers');
    }
    
    return multiplier * multiplicant;
};
/**
* This function calculates quotient between two numbers.
* @param {number} divident number being divided
* @param {number} divisor number used for dividing
* @returns {number} quotient
*/
export function divide (divident, divisor) {
    
    if(divisor === 0){ 
        throw new Error('Division by zero error!');
    }   
    return divident / divisor;
}
export default { add, subtract, multiply }
