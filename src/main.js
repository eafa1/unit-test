// src/main.js
import express from 'express';
import calc from './calc.js';
import { divide } from './calc.js';
const app = express();
const port = 3000;
const host = "localhost";

// Endpoint - GET http://localhost:3000/
app.get('/', (req, res) => {
res.send('Hello world');
});

app.get('/add', (req, res) => {
const a = parseInt(req.query.a);
const b = parseInt(req.query.b);
res.send(calc.add(a,b).toString());
});
app.get('/mult', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(calc.multiply(a,b).toString());
});
app.get('/sub', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(calc.subtract(a,b).toString());
});
app.get('/div', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(divide(a,b).toString());
});

app.listen(port, host, () => {
console.log(`Server: http://localhost:${port}`);
});