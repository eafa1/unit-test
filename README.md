# Node task unit testing
- [Node task unit testing](#node-task-unit-testing)
  - [Description](#description)
  - [Add your files](#add-your-files)
  - [Installation steps](#installation-steps)
  - [Using tests](#using-tests)
  - [Collaborate with your team](#collaborate-with-your-team)
  - [Test and Deploy](#test-and-deploy)
- [Overview of this project](#overview-of-this-project)
  - [Description](#description-1)
  - [Usage](#usage)
  - [Support](#support)
  - [Authors and acknowledgment](#authors-and-acknowledgment)
  - [License](#license)
  - [Project status](#project-status)



## Description

This project contains node tast for unit testing.

Includes function for add, subtraction, multiplication and division.

Some text in this readme file are placeholders to be replaced in future development phases.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/eafa1/unit-test.git
git branch -M main
git push -uf origin main
```

## Installation steps

1. Copy the repository
```
git clone https://gitlab.com/eafa1/unit-test.git
cd ./hello_express
npm i
```
2. Install dependencies
- [ ] [Install NodeJS](https://nodejs.org/en/download/)   
3. Test NodeJS commands
```
node --version
npm --version
npx --version
```
4. Start the app
   
Replace x and y with numbers for functions.
```
cd ./unit-test
node ./src/main.js

curl http://localhost:3000/
curl "http://127.0.0.1:3000/multiply?a=x&b=y" 
```

## Using tests
```
cd ./unit-test
npm run test
```

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Overview of this project

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
For support or help with this project use google.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
Development of this project has seized completely at the end of 2024.
